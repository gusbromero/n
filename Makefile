all:	swmm slice R

swmm:
	make -C swmm_engine

slice:	SWMM/sanca_v2.inp
	cd SWMM && ../slice_SWMM_input.sh sanca_v2.inp && cd ..
	
R:
	Rscript main_script.R       # Generate water quantity plots
	Rscript R-scripts/quality.R # Generate water quality plots

clean:
	make clean -C swmm_engine
	rm -fr SWMM/*.txt
	rm -fr *.emf
	rm -fr Figures/*Figure*.emf
