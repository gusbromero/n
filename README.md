# Install:

```sh
sudo groupinstall "Development tools" or apt-get install build-essential
sudo dnf install glibc-devel
sudo pip install swmmtoolbox
sudo R -e 'install.packages("ggplot2",repos="http://cran.us.r-project.org")'
sudo R -e 'install.packages("igraph",repos="http://cran.us.r-project.org")'
```
