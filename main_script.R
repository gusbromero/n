#> IP <- link_v[,2:ncol(link_v)]*link_d[,2:ncol(link_d)]
#> source("gusLib.R")
#> data.frame(Time=link_v[1
#> data.frame(Time=link_v[1],IP)
#> plotTilesT02(IP)
#> source("graph.R")
#> 
#
#
#> levels_ordered<-as.character(tc$Link)
#> levels_ordered[levels_ordered == "1"] <- "X1"
#> plotTilesT02(IP,levels_order=levels_ordered)
#
#
#>gerar precipitacao
#>salvar precipitacao
#>plotar precipitacao
#>simular
#>gerar arquivos de saida do swmm
#>extrair tc da bacia
#>pegar levels ordenados do bacia


##CWD deve ser no SWMM ##

library(ggplot2)
library(scales)
library(plyr)
library(devEMF)


source("R-scripts/swmm.R")
source("R-scripts/graph.R")
source("R-scripts/plotLib.R")
source("R-scripts/IDF.R")


rainfall <- makePivot(2,130,5,0.5) #Chuva de Projeto: 130 min (~= Tc da bacia), DeltaT = 5 min, Tr = 2 anos, pico de precipitacap em 0.5 == 50 %)
rainfall_ <- rainfall #copia da serie de precipitacao que nao sera alterada por convert2SWMM() e que sera usada, portanto, para gerar p01
p01 <- ggplot(rainfall_,aes(x=rainfall_$Time,y=rainfall_$Rainfall)) + geom_bar(stat="identity") + scale_x_datetime(date_breaks = "1 hour", labels=date_format("%H:%M")) + theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust=0.5)) + xlab("Tempo (h)") + ylab("P (mm/h)") #plotar precipitacao
#ggsave(filename="p1dia_5min_TR2anos.pdf",plot=p01) #salvar em PDF
rainfall <- convert2SWMM(rainfall)#converte para formato de precipitacao que o SWMM entende
write.table(rainfall,"ts05.dat",quote=FALSE,row.names=FALSE,col.names=FALSE)
swmm_command <- "swmm_engine/swmm5 SWMM/sanca_v2.inp SWMM/sanca_v2.rpt SWMM/sanca_v2.out"
system(swmm_command) #simulacao
# === CHAIN ===
link_v<-swmmGetLinkFlowVelocity("SWMM/sanca_v2.out") #dados simulacao velocidade nos links
link_d<-swmmGetLinkFlowDepth("SWMM/sanca_v2.out") #dados sumlacao profundidade lamina dagua
IP <- link_v[,2:ncol(link_v)]*link_d[,2:ncol(link_d)] #calculo do IP=h*v
IP <- data.frame(Time=link_v[1],IP) #cola de volta o timestamp das series, retirado para o calculo da IP
basin <- basinGraph()
tc <- computeTc(basin)
tc <- arrange(tc,Tc)
levels_ordered<-as.character(tc$Link)
levels_ordered[levels_ordered == "1"] <- "X1" #solucao de contorno para nomenclatura atual do link do exutorio
p02<-plotTilesT02(IP,levels_order=levels_ordered) #plot do fingerprint da bacia
#multiplot(p01,p02,cols=1) #imprime grafico e precipitacao e lajotas
link_Q<-swmmGetLinkFlowrate("SWMM/sanca_v2.out")
p03<-plotTilesT04(link_Q,levels_order=levels_ordered) #plot do fingerprint da bacia

emf("P_Tr2anos_130min.emf")
p01
dev.off()

emf("IP_Tr2anos_6horas.emf")
p02
dev.off()

emf("Q_Tr2anos_6horas.emf")
p03
dev.off()


rainfall <- makePivot(5,130,5,0.5) #Chuva de Projeto: 130 min (~= Tc da bacia), DeltaT = 5 min, Tr = 5 anos, pico de precipitacap em 0.5 == 50 %)
rainfall_ <- rainfall #copia da serie de precipitacao que nao sera alterada por convert2SWMM() e que sera usada, portanto, para gerar p01
p01 <- ggplot(rainfall_,aes(x=rainfall_$Time,y=rainfall_$Rainfall)) + geom_bar(stat="identity") + scale_x_datetime(date_breaks = "1 hour", labels=date_format("%H:%M")) + theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust=0.5)) + xlab("Tempo (h)") + ylab("P (mm/h)") #plotar precipitacao
#ggsave(filename="p1dia_5min_TR2anos.pdf",plot=p01) #salvar em PDF
rainfall <- convert2SWMM(rainfall)#converte para formato de precipitacao que o SWMM entende
write.table(rainfall,"ts05.dat",quote=FALSE,row.names=FALSE,col.names=FALSE)
swmm_command <- "swmm_engine/swmm5 SWMM/sanca_v2.inp SWMM/sanca_v2.rpt SWMM/sanca_v2.out"
system(swmm_command) #simulacao
# === CHAIN ===
link_v<-swmmGetLinkFlowVelocity("SWMM/sanca_v2.out") #dados simulacao velocidade nos links
link_d<-swmmGetLinkFlowDepth("SWMM/sanca_v2.out") #dados sumlacao profundidade lamina dagua
IP <- link_v[,2:ncol(link_v)]*link_d[,2:ncol(link_d)] #calculo do IP=h*v
IP <- data.frame(Time=link_v[1],IP) #cola de volta o timestamp das series, retirado para o calculo da IP
basin <- basinGraph()
tc <- computeTc(basin)
tc <- arrange(tc,Tc)
levels_ordered<-as.character(tc$Link)
levels_ordered[levels_ordered == "1"] <- "X1" #solucao de contorno para nomenclatura atual do link do exutorio
p02<-plotTilesT02(IP,levels_order=levels_ordered) #plot do fingerprint da bacia
#multiplot(p01,p02,cols=1) #imprime grafico e precipitacao e lajotas
link_Q<-swmmGetLinkFlowrate("SWMM/sanca_v2.out")
p03<-plotTilesT04(link_Q,levels_order=levels_ordered) #plot do fingerprint da bacia

emf("P_Tr5anos_3horas.emf")
p01
dev.off()

emf("IP_Tr5anos_3horas.emf")
p02
dev.off()

emf("Q_Tr5anos_3horas.emf")
p03
dev.off()


rainfall <- makePivot(10,130,5,0.5) #Chuva de Projeto: 130 min (~= Tc da bacia), DeltaT = 5 min, Tr = 10 anos, pico de precipitacap em 0.5 == 50 %)
rainfall_ <- rainfall #copia da serie de precipitacao que nao sera alterada por convert2SWMM() e que sera usada, portanto, para gerar p01
p01 <- ggplot(rainfall_,aes(x=rainfall_$Time,y=rainfall_$Rainfall)) + geom_bar(stat="identity") + scale_x_datetime(date_breaks = "1 hour", labels=date_format("%H:%M")) + theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust=0.5)) + xlab("Tempo (h)") + ylab("P (mm/h)") #plotar precipitacao
#ggsave(filename="p1dia_5min_TR2anos.pdf",plot=p01) #salvar em PDF
rainfall <- convert2SWMM(rainfall)#converte para formato de precipitacao que o SWMM entende
write.table(rainfall,"ts05.dat",quote=FALSE,row.names=FALSE,col.names=FALSE)
swmm_command <- "swmm_engine/swmm5 SWMM/sanca_v2.inp SWMM/sanca_v2.rpt SWMM/sanca_v2.out"
system(swmm_command) #simulacao
# === CHAIN ===
link_v<-swmmGetLinkFlowVelocity("SWMM/sanca_v2.out") #dados simulacao velocidade nos links
link_d<-swmmGetLinkFlowDepth("SWMM/sanca_v2.out") #dados sumlacao profundidade lamina dagua
IP <- link_v[,2:ncol(link_v)]*link_d[,2:ncol(link_d)] #calculo do IP=h*v
IP <- data.frame(Time=link_v[1],IP) #cola de volta o timestamp das series, retirado para o calculo da IP
basin <- basinGraph()
tc <- computeTc(basin)
tc <- arrange(tc,Tc)
levels_ordered<-as.character(tc$Link)
levels_ordered[levels_ordered == "1"] <- "X1" #solucao de contorno para nomenclatura atual do link do exutorio
p02<-plotTilesT02(IP,levels_order=levels_ordered) #plot do fingerprint da bacia
#multiplot(p01,p02,cols=1) #imprime grafico e precipitacao e lajotas
link_Q<-swmmGetLinkFlowrate("SWMM/sanca_v2.out")
p03<-plotTilesT04(link_Q,levels_order=levels_ordered) #plot do fingerprint da bacia

emf("P_Tr10anos_3horas.emf")
p01
dev.off()

emf("IP_Tr10anos_3horas.emf")
p02
dev.off()

emf("Q_Tr10anos_3horas.emf")
p03
dev.off()


rainfall <- makePivot(50,130,5,0.5) #Chuva de Projeto: 130 min (~= Tc da bacia), DeltaT = 5 min, Tr = 50 anos, pico de precipitacap em 0.5 == 50 %)
rainfall_ <- rainfall #copia da serie de precipitacao que nao sera alterada por convert2SWMM() e que sera usada, portanto, para gerar p01
p01 <- ggplot(rainfall_,aes(x=rainfall_$Time,y=rainfall_$Rainfall)) + geom_bar(stat="identity") + scale_x_datetime(date_breaks = "1 hour", labels=date_format("%H:%M")) + theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust=0.5)) + xlab("Tempo (h)") + ylab("P (mm/h)") #plotar precipitacao
#ggsave(filename="p1dia_5min_TR2anos.pdf",plot=p01) #salvar em PDF
rainfall <- convert2SWMM(rainfall)#converte para formato de precipitacao que o SWMM entende
write.table(rainfall,"ts05.dat",quote=FALSE,row.names=FALSE,col.names=FALSE)
swmm_command <- "swmm_engine/swmm5 SWMM/sanca_v2.inp SWMM/sanca_v2.rpt SWMM/sanca_v2.out"
system(swmm_command) #simulacao
# === CHAIN ===
link_v<-swmmGetLinkFlowVelocity("SWMM/sanca_v2.out") #dados simulacao velocidade nos links
link_d<-swmmGetLinkFlowDepth("SWMM/sanca_v2.out") #dados sumlacao profundidade lamina dagua
IP <- link_v[,2:ncol(link_v)]*link_d[,2:ncol(link_d)] #calculo do IP=h*v
IP <- data.frame(Time=link_v[1],IP) #cola de volta o timestamp das series, retirado para o calculo da IP
basin <- basinGraph()
tc <- computeTc(basin)
tc <- arrange(tc,Tc)
levels_ordered<-as.character(tc$Link)
levels_ordered[levels_ordered == "1"] <- "X1" #solucao de contorno para nomenclatura atual do link do exutorio
p02<-plotTilesT02(IP,levels_order=levels_ordered) #plot do fingerprint da bacia
#multiplot(p01,p02,cols=1) #imprime grafico e precipitacao e lajotas
link_Q<-swmmGetLinkFlowrate("SWMM/sanca_v2.out")
p03<-plotTilesT04(link_Q,levels_order=levels_ordered) #plot do fingerprint da bacia

emf("P_Tr50anos_130min.emf")
p01
dev.off()

emf("IP_Tr50anos_6horas.emf")
p02
dev.off()

emf("Q_Tr50anos_6horas.emf")
p03
dev.off()
