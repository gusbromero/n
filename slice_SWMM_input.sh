#!/bin/bash

## Sections to be sliced from .inp SWMM input file.
## The data contained in these sections will be used
## to build a weigthed directional graph and, after that, 
## a specific algorithm will calculate the concentraction time
## for each vertex. The graph's vertices are the control points in
## our SWMM Model.

edges_filename=edges.txt	## CONDUITS Section	
coord_filename=coordinates.txt  ## COORDINATES Section
verti_filename=vertices.txt	## JUNCTIONS Section
outfl_filename=outfalls.txt	## OUTFALLS Section

if [ ! -f ${1} ]; then 
  echo "Verify SWMM .inp filename!"
  exit 1
fi

dos2unix $1

echo "SWMM .inp File >> ${1} <<"

echo -n "Slicing Graph Edges data from [CONDUITS] Section: "
awk '/\[CONDUITS\]/,/^$/ {if ($0 !~ /;/ && $0 !~ /^$/) print $0}'    ${1} > ${edges_filename} && echo "${edges_filename} created."

echo -n "Slicing Graph Vertices and Outfalls X-Y Coord. data from [COORDINATES] Section: "
awk '/\[COORDINATES\]/,/^$/ {if ($0 !~ /;/ && $0 !~ /^$/) print $0}' ${1} > ${coord_filename} && echo "${coord_filename} created."

echo -n "Slicing Graph Vertices Z Coordinate data from [JUNCTIONS] Section: "
awk '/\[JUNCTIONS\]/,/^$/ {if ($0 !~ /;/ && $0 !~ /^$/) print $0}'   ${1} > ${verti_filename} && echo "${verti_filename} created."

echo -n "Slicing Graph Outfalls Z Coordinate data from [OUTFALLS] Section: "
awk '/\[OUTFALLS\]/,/^$/ {if ($0 !~ /;/ && $0 !~ /^$/) print $0}'    ${1} > ${outfl_filename} && echo "${outfl_filename} created."
