### Como usar isso?
### > source("graph.R") 
### > basin <- basinGraph(), para gerar a bacia 
### Note que os arquivos de input para basinGraph() devem ser a
### ajustados na propria funcao.
### Calcular o Tempo de Concentração (Tc) para cada vértice do grafo:
### > basin <- computeTc(basin)
### Os Tcs para cada vértice (ou nó) estão no vetor retornado por:
### > V(basin)$tc
###
### Depois, monte um data.frame para ordernar os Tcs:
### > tc_df <- data.frame(V=1:length(V(basin)$tc),Tc=V(basin)$tc/60) 
### - Tc foi divido por 60 para "sair" em horas.
### Ordene tc_df segundo a coluna Tc:
### > library(plyr)
### > tc_df <- arrange(tc_df,Tc)
### Pegue os nós ordenados pelo Tc da seguinte forma:
### > tc_df$V
### Pronto!
### Se quiser plotar o grafo:
### tkplot(basin,vertex.frame.color="black",vertex.color="white",vertex.label.cex=0.8,edge.color="blue")

require(igraph)

basinGraph <- function()
{
 ### Arquivos de input, recortados a partir do
 ### arquivo de input do SWMM, *.inp
 ### Os arquivos foram gerados de acordo com as
 ### seções no arquivo de *.inp.

 edges_filename <-"SWMM/edges.txt"       # Section Conduits
 coord_filename <-"SWMM/coordinates.txt" # Section Coodinates
 verti_filename <-"SWMM/vertices.txt"    # Section Junctions
 outfl_filename <-"SWMM/outfalls.txt"    # Section Outfalls
  
 table<-read.table(edges_filename,skip=1,header=FALSE)
 edges<-table[2:3]
 edges_names<-table[1]
 edges_length<-table[4]
 # Edges
 edges<-data.frame(edges,edges_names,edges_length)
 colnames(edges) <- c("inlet","outlet","name","length")

 # Junctions + Outfalls Elevation
 table<-read.table(verti_filename,skip=1,header=FALSE)
 junctions_elevation<-table[2]
 colnames(junctions_elevation) <- "elevation"

 table<-read.table(outfl_filename,skip=1,header=FALSE)
 outfalls_elevation<-table[2] 
 colnames(outfalls_elevation) <- "elevation" 
 vertices_elevation <- rbind(junctions_elevation,outfalls_elevation)

 table <- read.table(coord_filename,skip=1,header=FALSE)
 vertices_name <- table[1]
 colnames(vertices_name)    <- "name"
 vertices_coord_x <- table[2]
 colnames(vertices_coord_x) <- "X" # Talvez você se pergunte o porquê de um 'x' maiúsculo. A resposta é
				   # que isso deve ser um bug no igraph 1.0.0. No igraph 0.7.0, se esse atributo
				   # fosse chamado de 'x' minúsculo, não teria problema. Mas no 1.0.0 isso é um problema!
 vertices_coord_y <- table[3]
 colnames(vertices_coord_y) <- "y"
 vertices_w      <- data.frame(w=rep(0,nrow(table)))
 vertices_source <- data.frame(source=rep(0,nrow(table)))
 vertices_slope  <- data.frame(slope=rep(0,nrow(table))) 
 vertices_tc 	 <- data.frame(tc=rep(0,nrow(table)))

 # Vertices 
 vertices<-data.frame(vertices_name,vertices_elevation,vertices_coord_x,vertices_coord_y,vertices_w,vertices_source,vertices_slope,vertices_tc)  

 # Build graph
 basin <- graph.data.frame(edges,directed=TRUE,vertices=vertices)
 sources <- getSources(basin)
 basin <- setSources(basin,sources) 
 basin <- setWeight(basin)

 return(basin)
}

getSources <- function(basin)
{
 v <- get.adjlist(basin, mode=c("in"))
 sources <- which(sapply(v, length) == 0) 
}

getOutfall <- function(basin)
{
 # Currently, just one outfall is supported.
 # If the basin contains more than 1 outfall,
 # this function will return them, but 
 # further methods will fail!

 v <- get.adjlist(basin, mode=c("out"))
 outfall <- which(sapply(v,length) == 0 )
}

setSources <- function(basin, sources)
{
 V(basin)$source[sources] <- sources 
 basin
}

weightVertices <- function(basin, v_source)
{  

  cat("v_source=",v_source,"\n")

  while(1 == 1) #FIXME, it can be done as a single statement.
  {

  v_next <- as.vector(basin[[v_source]][[1]])

#  print(v_source)
#  print(v_next)
  
  if( identical(v_next, integer(0)) == TRUE) 
# if(v_next == NA)
  {
#  e <- incident(basin,v_source,mode=c("out"))  
#  len <- E(basin)$length[e]

   print("ending")
#   if( V(basin)$w[v_next] < ( V(basin)$w[v_source] + len ) )
#    V(basin)$w[v_next] <- V(basin)$w[v_source] + len 
   
#   cat("Updating weight of OUTFALL",V(basin)$name[v_next],"to",V(basin)$w[v_next],"\n")
   print("Outfall reached. No more walk.\n")
   return(basin)
  }

  else #FIXME, read above FIXME 
  {

  e <- incident(basin,v_source,mode=c("out"))  
  len <- E(basin)$length[e]
 
  print("normal")
  if( V(basin)$w[v_next] < ( V(basin)$w[v_source] + len ) )
  { 
   V(basin)$w[v_next] <- V(basin)$w[v_source] + len   
   V(basin)$source[v_next] <- V(basin)$source[v_source]
   cat("Updating weight of VERTEX",V(basin)$name[v_next],"to",V(basin)$w[v_next],"o> ",V(basin)$source[v_next],"\n") 
  }
  }
   v_source <- v_next
  }
}

setWeight <- function(basin)
{
 for (i in 1:vcount(basin))
  basin <- weightVertices(basin, i)

  return(basin)
}

computeTc <- function(basin)
{
 link_name <-c()
 link_tc   <-c()

 for (vertex in 1:vcount(basin))
 {
  if (V(basin)$source[vertex] != vertex)
  {
   vertex_source <- V(basin)$source[vertex] 
 
   vertex_X  <- V(basin)$X[vertex]
   vertex_source_X <- V(basin)$X[vertex_source]
  
   vertex_Y <- V(basin)$y[vertex] 
   vertex_source_Y <- V(basin)$y[vertex_source]

   X <- abs(vertex_X - vertex_source_X)
   Y <- abs(vertex_Y - vertex_source_Y) 

   ## Compute Distance (m), if input unit is CMS
   Distance <- sqrt(X^2 + Y^2)

   vertex_Elevation <- V(basin)$elevation[vertex]
   vertex_source_Elevation <- V(basin)$elevation[vertex_source]

   ## Compute Elevation (m), if input unit is CMS
   Elevation <- abs(vertex_Elevation - vertex_source_Elevation)

   ## Compute Slope
#  print(Elevation)
#  print(Distance) 
   Slope <- Elevation / Distance
#  print(Slope)
   V(basin)$slope[vertex] <- Slope
   V(basin)$tc[vertex] <- Kirpich(V(basin)$w[vertex],V(basin)$slope[vertex])	

   cat("Vertex",vertex,"\t( ",V(basin)$name[vertex]," )","\thas SLOPE set to",round(Slope,3),"\tand LENGTH of",V(basin)$w[vertex],"\tmeter(s);", "Tc by Kirpich =",V(basin)$tc[vertex],"minute(s)\n")

   out_going_edge <- as.numeric(incident_edges(basin, vertex, mode=c("out")))
   if ( ! is.na(out_going_edge) )#testa se nao eh o exutorio pq se for nao existe link de saida
   {  
    link_name <- c(link_name, E(basin)[out_going_edge]$name)
    link_name <- gsub("-",".",link_name)
    link_tc   <- c(link_tc, V(basin)$tc[vertex])
   }

  }
  else
  {
   V(basin)$tc[vertex] <- 0; 
   cat("Vertex",vertex,"\t( ",V(basin)$name[vertex]," )","\thas SLOPE set to",round(Slope,3),"\tand LENGTH of",V(basin)$w[vertex],"\tmeter(s);", "Tc by Kirpich =",V(basin)$tc[vertex],"minute(s)\n")

   out_going_edge <- as.numeric(incident_edges(basin, vertex, mode=c("out")))
   if ( ! is.na(out_going_edge) ) #testa se nao eh o exutorio pq se for nao existe link de saida
   {  
    link_name <- c(link_name, E(basin)[out_going_edge]$name)
    link_name <- gsub("-",".",link_name)
    link_tc <- c(link_tc, V(basin)$tc[vertex])

   }

  }
 }

 df <- data.frame(Link=link_name,Tc=link_tc)
 return(df)

#return(basin)
}

Kirpich <- function(L,S)
{
 # L -> Comprimento do talvegue (m)


 L <- L/1000 # Formula needs length in (km)

 return(((3.989*L)^0.77)*S^(-0.385)) # Buscar artigo de referencia.

}

